variable "gcp_project_id" {
  description = "The gcp project id that will host our infrastructure"
  type        = string
}

variable "gitlab_group_name" {
  description = "The group name configured to use the provided infrastructure"
  type        = string
}

variable "gcp_credentials" {
  description = "The content of a gcp json credential file"
  type        = string
}

variable "gitlab_token" {
  description = "Gitlab Oauth token with api access granted"
  type        = string
}

variable "desired_state" {
  description = "Desired state of the build infrastructure (RUNNING or TERMINATED)"
  type        = string
  validation {
    condition     = var.desired_state == "RUNNING" || var.desired_state == "TERMINATED"
    error_message = "This desired_state value does not exists."
  }
  default = "TERMINATED"
}
