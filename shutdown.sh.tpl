#!/usr/bin/env bash

docker-machine ls -q | parallel -j10 docker-machine rm --force {}
gitlab-runner verify --delete
gitlab-runner unregister --url https://gitlab.com/ --token ${group_registration_token} --all-runners