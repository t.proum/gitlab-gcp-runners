provider "google" {
  credentials = var.gcp_credentials
  project     = var.gcp_project_id
  region      = "europe-west1"
  zone        = "europe-west1-b"
}

provider "gitlab" {
  token = var.gitlab_token
}

data "gitlab_group" "default" {
  full_path = var.gitlab_group_name
}

resource "google_project_service" "cloud_resource_manager" {
  service            = "cloudresourcemanager.googleapis.com"
  disable_on_destroy = false
}

resource "google_service_account" "runner" {
  account_id = "gitlab-runner"
}

resource "google_project_iam_member" "runner_compute_admin" {
  project    = var.gcp_project_id
  member     = "serviceAccount:${google_service_account.runner.email}"
  role       = "roles/compute.admin"
  depends_on = [google_project_service.cloud_resource_manager]
}

resource "google_project_iam_member" "runner_iam_service_account_user" {
  project    = var.gcp_project_id
  role       = "roles/iam.serviceAccountUser"
  member     = "serviceAccount:${google_service_account.runner.email}"
  depends_on = [google_project_service.cloud_resource_manager]
}

resource "google_project_iam_member" "runner_storage_reader" {
  project    = var.gcp_project_id
  role       = "roles/storage.objectViewer"
  member     = "serviceAccount:${google_service_account.runner.email}"
  depends_on = [google_project_service.cloud_resource_manager]
}

data "template_file" "startup_script" {
  template = file("startup.sh.tpl")
  vars = {
    group_registration_token = data.gitlab_group.default.runners_token
    gcp_project_id           = var.gcp_project_id
  }
}

data "template_file" "shutdown_script" {
  template = file("shutdown.sh.tpl")
  vars = {
    group_registration_token = data.gitlab_group.default.runners_token
  }
}

resource "google_storage_bucket" "default" {
  name     = "${var.gcp_project_id}-scripts"
  location = "EU"
}

resource "google_storage_bucket_object" "startup" {
  bucket  = google_storage_bucket.default.name
  name    = "startup.sh"
  content = data.template_file.startup_script.rendered
}

resource "google_storage_bucket_object" "shutdown" {
  bucket  = google_storage_bucket.default.name
  name    = "shutdown.sh"
  content = data.template_file.shutdown_script.rendered
}

resource "google_compute_instance" "runner" {
  machine_type = "e2-medium"
  name         = "gitlab-runner"
  boot_disk {
    initialize_params {
      image = "ubuntu-2004-lts"
    }
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
  service_account {
    email  = google_service_account.runner.email
    scopes = ["cloud-platform"]
  }
  metadata = {
    startup-script-url  = "gs://${google_storage_bucket.default.name}/${google_storage_bucket_object.startup.name}"
    shutdown-script-url = "gs://${google_storage_bucket.default.name}/${google_storage_bucket_object.shutdown.name}"
  }
  desired_status = var.desired_state
}