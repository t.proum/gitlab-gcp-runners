terraform {
  backend "http" {
  }
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.9.1"
    }
    google = {
      source  = "hashicorp/google"
      version = "4.10.0"
    }
  }
}
