#!/usr/bin/env bash

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
apt update
apt install -y curl docker.io gitlab-runner parallel

base=https://github.com/docker/machine/releases/download/v0.16.0 \
&& curl -L "$base/docker-machine-$(uname -s)-$(uname -m)" >/tmp/docker-machine \
&& mv /tmp/docker-machine /usr/local/bin/docker-machine \
&& chmod +x /usr/local/bin/docker-machine

gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "${group_registration_token}" \
  --executor "docker+machine" \
  --docker-image ruby:2.6 \
  --description "gcp-runners" \
  --tag-list "docker,gcp" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

cat << EOF > /etc/gitlab-runner/config.toml
concurrent = 16
check_interval = 0
[session_server]
  session_timeout = 1800
[[runners]]
  name = "gcp-runners"
  url = "https://gitlab.com/"
  token = "${group_registration_token}"
  executor = "docker+machine"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ruby:2.6"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock" ]
    shm_size = 0
  [runners.machine]
    IdleCount = 3
    MachineDriver = "google"
    MachineName = "gcp-executor-%s"
    MachineOptions = [
       "google-project=tpr-project",
       "google-zone=europe-west1-b"
    ]
EOF