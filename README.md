# Gitlab gcp runners

This repository aims to help to bootstrap a simple gcp auto-scale runner to allows building stuff with gitlab


<!--- BEGIN_TF_DOCS --->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | 3.9.1 |
| <a name="requirement_google"></a> [google](#requirement\_google) | 4.10.0 |
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_desired_state"></a> [desired\_state](#input\_desired\_state) | Desired state of the build infrastructure (RUNNING or TERMINATED) | `string` | `"TERMINATED"` | no |
| <a name="input_gcp_credentials"></a> [gcp\_credentials](#input\_gcp\_credentials) | The content of a gcp json credential file | `string` | n/a | yes |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | The gcp project id that will host our infrastructure | `string` | n/a | yes |
| <a name="input_gitlab_group_name"></a> [gitlab\_group\_name](#input\_gitlab\_group\_name) | The group name configured to use the provided infrastructure | `string` | n/a | yes |
| <a name="input_gitlab_token"></a> [gitlab\_token](#input\_gitlab\_token) | Gitlab Oauth token with api access granted | `string` | n/a | yes |
## Outputs

No outputs.
<!--- END_TF_DOCS --->